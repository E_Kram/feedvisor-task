﻿var str='';
var targ_url;
var email_login = 'feedvisor_task'
var email_password = '5t4r3e2w1';

module.exports = {

    'case one: mail.ru search service' : function (browser) {
      browser
        .url('https://mail.ru/')
        .waitForElementVisible('body', 1000)
        .waitForElementVisible('#search', 1000)
        .click('#q')
		.setValue('#q', 'великая китайская стена')
        .click('button[type=submit]')
        .waitForElementVisible('#section-web', 1000)
        .assert.containsText('#section-web', 'великая китайская стена', "Страница содержит запрос 'великая китайская стена'")
        .waitForElementVisible('#js-result_2', 1000)
        .assert.containsText('#js-result_2 > div > div.SnippetResultInfo-info.SnippetResult-urlWithImage > span > a', 'wikiway.com/','В поисковой выдаче 2й результат с ресурса "https://wikiway.com/"')
        .saveScreenshot(`./output/search.png`)
        .end();
    },

    'case two: mail.ru mailbox checking' : function (browser) {
      browser
		//открытие почтового ящика: ввод логина и пароля
        .url('https://mail.ru/')
        .waitForElementVisible('input[name=login]', 5000)
		.setValue('input[name=login]', email_login)
        .waitForElementVisible('button[data-testid=enter-password]', 1000)
        .click('button[data-testid=enter-password]')
        .waitForElementVisible('input[name=password]', 1000)
		.setValue('input[name=password]', email_password)
        .click('button[data-testid=login-to-mail]')		
		
		.pause(5000)	
        .waitForElementVisible('#app-canvas', 20000, "Авторизация и вход в почту: успешно")
		
		//проверка того, что в ящике 3 письма 
		browser.elements('css selector', 'a[data-id]', function(elements) {
		  browser.assert.equal(elements.value.length, 3, "В ящике 3 письма");
		})
			
		//все прочитанные письма пометить как непрочитанные
		browser.elements('css selector', 'span', function(elements) {
		  elements.value.forEach(function(element){
			browser.elementIdAttribute(element.ELEMENT, 'title', function(attribute) {
				if(attribute.value=='Пометить непрочитанным'){
					browser.elementIdClick(element.ELEMENT);
				}
			});
		  });
		})				
		.pause(1000)	
		
		//проверить, что все письма помечены как непрочитанные	
		.execute(function() {	
			browser.expect.element('#g_mail_events').text.to.be("3");
			return true;
		},[],function(result){		
			browser.assert.equal(true, true, "Все письма помечены как непрочитанные");	
		})

		//открыть все письма в новом окне
		browser.elements('css selector', 'a[data-id]', function(elements) {
			elements.value.forEach(function(element){		
				browser.elementIdAttribute(element.ELEMENT, 'data-id', function(attribute) {
				
					targ_url = 'https://e.mail.ru/inbox/'+attribute.value+'/';
						  
					browser.execute(function(link) {											
						window.open(link,'_blank');
					},  [targ_url]);
					browser.pause(1000)
					  					
				});
			});
		})
		.pause(1000)
		
		//вернуться в окно почты
		.window_handles(function (result) {
            var handle = result.value[0];
            browser.switchWindow(handle);
        })
		.pause(1000)
		
		//проверить, что не осталось непрочитанных писем
		.execute(function() {	
			browser.expect.element('#g_mail_events').text.to.be("0");
			return true;
		},[],function(result){		
			browser.assert.equal(true, true, "Нет непрочитанных писем");	
		})
			
        .end();
    }
};